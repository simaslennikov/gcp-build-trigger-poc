# GCP Build Trigger POC

Ideally this will answer [this](https://stackoverflow.com/questions/70807901/unable-to-connect-from-google-cloud-to-bitbucket-repository-using-app-password).

## Steps

1. Follow [GCP documentation for building the trigger](https://cloud.google.com/build/docs/automating-builds/build-repos-from-bitbucket-cloud)
2. Follow [Step 2 of Bitbucket guide on creating webhooks](https://support.atlassian.com/bitbucket-cloud/docs/create-and-trigger-a-webhook-tutorial/)
3. Make a random change; check that Cloud Build is triggered

![](commits.png)

![](builds.png)
